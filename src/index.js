
//HTTP methods: GET, POST, PUT, DELETE

//Parameters:
//Query params > used for GET methods, visible on the API URL (filter, pagination, order...)
//Route params > PUT, DELETE. Identification of the data to be deleted, e.g. User1. request.param
//Body > POST, PUT. To create data, e.g. User.  request.body
//MongoDB (non relational)

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');

const routes = require('./routes');
const { setupWebSocket } = require('./websocket');

const app = express();
const server = http.Server(app);

setupWebSocket(server);

mongoose.connect('mongodb+srv://vanessaadm:Giovana1%40@cluster0-hk4gg.mongodb.net/NodeJsTraining?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

app.use(cors());
app.use(express.json());
app.use(routes);

server.listen(3333);